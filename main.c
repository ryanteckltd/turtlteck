#include <msp430.h>

/*
 *	TurtlTeck Robot Controller Code
 *  Copyright (C) 2014  Ryanteck LTD.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

unsigned int currentInstruction = 0;
char instructions[99];

flashLED() {
	//Code to blink the LED
	P1OUT ^= 0x01;
	_delay_cycles(200000);
	P1OUT ^= 0x01;
	_delay_cycles(200000);
}

checkDelay(lr) {
	if((P2IN & 0x40) == 0x00) {
		//Switch 1 is turned on
		_delay_cycles(15000);
	}
	if((P2IN & 0x80) == 0x00) {
		//Switch 2 is turned on
		_delay_cycles(30000);
	}
	if((P1IN & 0x02) == 0x00) {
		//Switch 3 is turned on
		_delay_cycles(45000);
	}
	if((P1IN & 0x04) == 0x00 && lr==1) {
		//Switch 4 is turned on
		_delay_cycles(100000);
	}
}

int main(void) {
	//Sweep
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    //Stop the clock!
    P2SEL = 0x00;

    //Directions!
    P1DIR = 0xC1;
    P2DIR = 0x30;
    //Pull up!
    P1REN = 0x3E;
    P2REN = 0xCF;
    //And pin out
    P1OUT = 0x3E;
    P2OUT = 0xCF;
    //Interrupt
    P1IE = 0x38;
    P2IE = 0x07;
    //Flags
    P1IFG = 0x01;
    P2IFG = 0x00;

    //Say the board is booted ready for use.
    flashLED();

    //Low power mode; engage!
    _BIS_SR(LPM3_bits + GIE);
}

//Up left right
//Thanks to Jim Darby for the advise on optimising array.
#pragma vector=PORT1_VECTOR
__interrupt void Port_1 (void)
{
	flashLED();

	if(currentInstruction >= 98) {
		P1OUT ^= 0x01;
		
	}
	else {
		if(P1IFG & 0x08) {
			instructions[currentInstruction++] = 'w';
		}

		if(P1IFG & 0x10) {
			instructions[currentInstruction++] = 's';
		}

		if(P1IFG & 0x20) {
			instructions[currentInstruction++] = 'a';
		}
	}
	P1IFG = 0x01;
	
}

#pragma vector=PORT2_VECTOR
__interrupt void Port_2 (void)
	{

	flashLED();

	if(currentInstruction >= 98) {
			P1OUT ^= 0x01;
		}
	else {
		if(P2IFG & 0x01) {
			instructions[currentInstruction++] = 'd';
		}
	}
	
	if(P2IFG & 0x02) {
		unsigned int i=0;
		while (i<currentInstruction) {
			flashLED();
			if(instructions[i] == 'w') {
				P1OUT ^= 0x80;
				P2OUT ^= 0x10;
				_delay_cycles(250000);
				checkDelay(0);
				P1OUT ^= 0x80;
				P2OUT ^= 0x10;
			}
			if(instructions[i] == 's') {
				P1OUT ^= 0x40;
				P2OUT ^= 0x20;
				_delay_cycles(250000);
				checkDelay(0);
				P1OUT ^= 0x40;
				P2OUT ^= 0x20;
			}
			if(instructions[i] == 'a') {
				P1OUT ^= 0x80;
				P2OUT ^= 0x20;
				_delay_cycles(100000);
				checkDelay(1);
				P1OUT ^= 0x80;
				P2OUT ^= 0x20;
			}
			if(instructions[i] == 'd') {
				P1OUT ^= 0x40;
				P2OUT ^= 0x10;
				_delay_cycles(100000);
				checkDelay(1);
				P1OUT ^= 0x40;
				P2OUT ^= 0x10;
			}
		i++;
		}
	}

	if(P2IFG & 0x04) {
		if(currentInstruction != 0) {
			currentInstruction = currentInstruction -1;
			instructions[currentInstruction] = 'l';

		}
		else {
		flashLED();
		currentInstruction = 8;
		instructions[0] = 'w';
		instructions[1] = 'w';
		instructions[2] = 's';
		instructions[3] = 's';
		instructions[4] = 'a';
		instructions[5] = 'd';
		instructions[6] = 'a';
		instructions[7] = 'd';
		flashLED();
		}
	}

	P2IFG = 0x00;
}

